const {User} = require('../models'); //DIISI DARI DIREKTORI MODEL NTAR

module.exports = {
    index(req, res){
        User.findAll({})
        .then(users => res.json({
            error : false,
            data : users
        }))
        .catch(error => res.json({
            error : true,
            data : [],
            error: error
        }));
    },

    create(req, res){
        const {name, username} = req.body;
        User.create({
            name, username
        })
        .then(users => res.status(201).json({
            error: false,
            data: users,
            message : "Data Berhasil Ditambahkan"
        }))
        .catch(error => res.json({
            error : true,
            data : [],
            error : error
        }));
    },

    update(req, res){
        const user_id = req.params.id; //Mengambil data id dari parameter ID

        const {name, username} = req.body;

        User.update({
            name, username
        }, {
            where : {
                id: user_id
            }
        })
        .then(user => res.status(201).json({
            error: false,
            data: user,
            message : "Data Berhasil Diupdate"
        }))
        .catch(error => res.json({
            error : true,
            error : error
        }));
    },

    destroy(req, res){
        const user_id = req.params.id; //Mengambil data id dari parameter ID

        User.destroy({
            where : {
                id: user_id
            }
        })
        .then(user => res.status(201).json({
            error: false,
            message : "Data Berhasil Dihapus"
        }))
        .catch(error => res.json({
            error : true,
            error : error
        }));
    }
}